//use num256::Uint256;
//extern crate hex;
//use hex::FromHex;
use clarity::{Address, Uint256};
use clarity::abi::{Token, encode_call};
use num_bigint::{BigUint};

//fn main() {
//    let n:Uint256 = Uint256::from(42124125_u64);
//    let mut v:Vec<u8> = n.to_bytes_le();
//    let mut r:Vec<u8> = Vec::new();
//    let ss: String = String::from("a9059cbb");
//    let mut m:Vec<u8> = FromHex::from_hex(ss).unwrap();
//
//    let a:Address = Address::parse_and_validate("0xEb3907eCad74a0013c259D5874AE7f22DcBcC95C").unwrap();
//    
//    r.resize(32 - v.len(), 0);
//    r.append(&mut v);
//    m.append(&mut r);
//
//    //m
//    println!("{:?}", a.as_bytes());
//}
//

fn main() {
    let mut v:Vec<Token> = Vec::new();
    let mut a:Token = Token::Address(Address::parse_and_validate("0xEb3907eCad74a0013c259D5874AE7f22DcBcC95C").unwrap());
    let mut t:Token = Token::Uint(Uint256(BigUint::from(4294967296u64)));
    v.push(a);
    v.push(t);
    //let c:Vec<u8> = encode_call("transfer(address,uint256)", v.as_slice()).unwrap();
    let c:Vec<u8> = encode_call("transfer(address,uint256)", v.as_slice()).unwrap();
    println!("{:?}", c);
}
